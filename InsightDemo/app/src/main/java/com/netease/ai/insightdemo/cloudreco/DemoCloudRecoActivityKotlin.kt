package com.netease.ai.insightdemo.cloudreco

import android.Manifest
import android.content.Context
import android.content.pm.ActivityInfo
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.annotation.StringRes
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.Toast
import com.netease.ai.insightdemo.InsightMessageManagerDispatch
import com.netease.ai.insightdemo.R
import com.netease.insightar.NEArInsight
import com.netease.insightar.callback.AbsArInsightDataCallback
import com.netease.insightar.callback.OnArInsightResultListener
import com.netease.insightar.entity.*
import com.netease.insightar.entity.message.*
import com.netease.insightar.entity.message.IAr3dEventMessage.*
import com.netease.insightar.exception.ArResourceNotFoundException
import kotlinx.android.synthetic.main.activity_demo_cloud_reco3.*

/**
 * @author zfxu
 * 通过NEArView以及NEArInsight类来实现云识别扫一扫的AR功能 kotlin代码实现
 */
class DemoCloudRecoActivityKotlin : AppCompatActivity() {

    companion object {
        const val TAG = "DemoCloudRecoActivity3"
        const val REQUEST_CODE_PERMISSION = 101
    }


    //标识当前显示的算法是否是云识别，如果识别到具体事件之后，该变量为false
    private var mIsCloud = true
    private var mCurrentShowEventID = ""

    private var mInsightMessageManagerDispatch: InsightMessageManagerDispatch? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_demo_cloud_reco3)

        //云识别不支持横屏，所以需要强制竖屏，也可以在manifest内写死
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT

        val isCameraGranted = isPermissionGranted(this, Manifest.permission.CAMERA)
        val isWriteGranted = isPermissionGranted(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
        val isAudioRecodeGranted = isPermissionGranted(this,
                Manifest.permission.RECORD_AUDIO)
        var permissionGranted: Boolean
        if (isCameraGranted && isWriteGranted && isAudioRecodeGranted) {
            permissionGranted = true
            Log.i(TAG, "onCreate CAMERA & WRITE permission is granted")
        } else {
            Log.i(TAG, "onCreate CAMERA or WRITE  permission is not granted")
            ActivityCompat.requestPermissions(this,
                    arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA,
                            Manifest.permission.RECORD_AUDIO), REQUEST_CODE_PERMISSION)
            permissionGranted = false
        }
        neArView3.create(true)

        mInsightMessageManagerDispatch = InsightMessageManagerDispatch(this@DemoCloudRecoActivityKotlin)

        if (permissionGranted) {
            loadData()
        }
    }

    private fun loadData() {
        //加载数据
        showWaitView()
        //对于云识别的加载，NEArInsight提供相关方法
        //该方法不仅会获取网络数据，也会去下载和解压数据
        NEArInsight.loadCloudRecoResource(mLoadCloudResourceCallback)
    }

    var mLoadCloudResourceCallback = object : AbsArInsightDataCallback<ArInsightRecoResult> {
        override fun onDownloadError(id: String?, code: Int, msg: String?) {
            showToast("下载失败...")
        }

        override fun onDownloadPause(id: String?) {
        }

        override fun onNetworkDataError(code: Int, msg: String?) {
            showToast("获取云识别数据失败")
        }

        override fun unZipStart(id: String?) {
        }

        override fun onDownloadStart(id: String?) {
        }

        override fun onDownloadResume(id: String?) {
        }

        override fun onDownloadSucc(id: String?) {
        }

        override fun onNetworkDataSucc(t: ArInsightRecoResult?) {
        }

        override fun unZipEnd(id: String?, destFilePath: String?) {
            hideWaitView()
            if (destFilePath == null) {
                showToast("解压失败")
                return
            }
            try {
                neArView3.doArShowView(id, destFilePath, true)
            } catch (e: ArResourceNotFoundException) {
                showToast("AR显示失败")
            }
        }

        override fun onDownloadProgress(id: String?, progress: Int) {
        }
    }

    private val mArResultListener = object : OnArInsightResultListener {
        override fun onArRenderResult(p0: ArInsightRenderResult?) {
            //返回渲染引擎加载资源的状态
        }

        override fun on3dEventMessage(message: IAr3dEventMessage?) {
            val type = message!!.type()
            when (type) {
                TYPE_EXECUTE_SCRIPTS -> executeScripts(message as ExecuteScript3dEventMessage)
                TYPE_SHARE -> share(message as Share3dEventMessage)
                TYPE_RELOAD_AR -> reLoadAr(message as Reload3dEventMessage)
                TYPE_SCREENSHOT -> screenShot()
                TYPE_CLOSE_AR -> closeAr()
                TYPE_OPEN_URL -> openUrl(message as OpenUrl3dEventMessage)
                else -> otherEventMessage(message as Common3dEventMessage)
            }
        }

        override fun onArEngineResult(result: ArInsightAlgResult?) {
        }
    }

    private fun otherEventMessage(message: Common3dEventMessage) {

    }

    private fun openUrl(openUrl3dEventMessage: OpenUrl3dEventMessage) {
        mInsightMessageManagerDispatch!!.openUrl(openUrl3dEventMessage)
    }

    private fun closeAr() {
        mInsightMessageManagerDispatch!!.closeAr()
    }

    private fun screenShot() {
        //截屏
        //当前AR视图，截屏可以保存当前bitmap到文件，
        //该函数是耗时的，外部最好放在非UI线程中处理，以免引起ANR
        mInsightMessageManagerDispatch!!.screenShot(neArView3, mCurrentShowEventID)
    }

    private fun reLoadAr(reload3dEventMessage: Reload3dEventMessage) {
        mInsightMessageManagerDispatch!!.reloadAr(
                neArView3, reload3dEventMessage) { pid -> cloudModeRecognition(pid) }
    }

    private fun cloudModeRecognition(pid: String?) {
        //识别到具体事件，需要去请求数据
        //也可以先去get，在download
//    NEArInsight.downloadAndUnzipNormalEventResource(**info**, **your-callback**)
//    NEArInsight.getSingleNormalEventResource(pid, **your-callback**)
        showWaitView()
        mCurrentShowEventID = pid!!
        NEArInsight.loadSingleNormalEventResource(pid, mLoadNormalEventCallback)
    }

    var mLoadNormalEventCallback = object : AbsArInsightDataCallback<ArInsightEventResult> {
        override fun onDownloadError(id: String?, code: Int, msg: String?) {
            showToast("事件下载失败")
        }

        override fun onDownloadPause(id: String?) {
        }

        override fun onNetworkDataError(code: Int, msg: String?) {
            showToast("事件获取失败")
        }

        override fun unZipStart(id: String?) {
        }

        override fun onDownloadStart(id: String?) {
        }

        override fun onDownloadResume(id: String?) {
        }

        override fun onDownloadSucc(id: String?) {
        }

        override fun onNetworkDataSucc(t: ArInsightEventResult?) {
        }

        override fun unZipEnd(id: String?, destFilePath: String?) {
            hideWaitView()
            if (TextUtils.isEmpty(destFilePath)) {
                showToast("事件解压失败")
                return
            }
            neArView3.doArShowView(id, destFilePath!!, false)
        }

        override fun onDownloadProgress(id: String?, progress: Int) {
            Log.i(TAG, "normal event download progress: $progress")
            pbProgress.progress = progress
            pbProgressTv.text = "数据加载中... $progress"
        }

    }

    private fun share(share3dEventMessage: Share3dEventMessage) {
        //分享
        mInsightMessageManagerDispatch!!.share(share3dEventMessage, mCurrentShowEventID)
    }

    private fun executeScripts(message: ExecuteScript3dEventMessage) {
        mInsightMessageManagerDispatch!!.executeScripts(message, neArView3)
    }


    private fun showWaitView() {
        waitLayout3.visibility = View.VISIBLE
        pbProgress.progress = 0
        pbProgressTv.text = "数据加载中... "
    }

    private fun hideWaitView() {
        waitLayout3.visibility = View.GONE
    }

    override fun onResume() {
        super.onResume()
        neArView3.resume()
    }

    override fun onPause() {
        super.onPause()
        neArView3.pause()
    }

    override fun onStart() {
        super.onStart()
        neArView3.start()
        neArView3.registerArInsightResultListener(mArResultListener)
    }

    override fun onStop() {
        super.onStop()
        neArView3.stop()
        neArView3.unregisterArInsightResultListener(mArResultListener)
    }

    override fun onDestroy() {
        super.onDestroy()
        neArView3.destroy()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        neArView3.onBackPressed()
    }

    private fun isPermissionGranted(context: Context, permission: String): Boolean {
        return ContextCompat.checkSelfPermission(context,
                permission) == PackageManager.PERMISSION_GRANTED
    }

    private fun showToast(s: String) {
        showToast(s, true)
    }

    fun showToast(str: String, isShort: Boolean) {
        Toast.makeText(this, str, if (isShort) Toast.LENGTH_SHORT else Toast.LENGTH_LONG).show()
    }

    fun showToast(@StringRes str: Int, isShort: Boolean) {
        Toast.makeText(this, str, if (isShort) Toast.LENGTH_SHORT else Toast.LENGTH_LONG).show()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>,
                                            grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == REQUEST_CODE_PERMISSION) {
            var isPermissionGranted = true
            var per: String
            for (i in permissions.indices) {
                per = permissions[i]
                if (per == Manifest.permission.WRITE_EXTERNAL_STORAGE || per == Manifest.permission.CAMERA || per == Manifest.permission.RECORD_AUDIO) {
                    if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                        isPermissionGranted = false
                        break
                    }
                }
            }
            if (!isPermissionGranted) {
                showToast("你已禁止权限...", true)
                finish()
                return
            }
            neArView3.onRequestPermissionsResult(true)
            loadData()
        }
    }

}
