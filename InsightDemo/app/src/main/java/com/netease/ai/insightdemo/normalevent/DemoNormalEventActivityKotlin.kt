package com.netease.ai.insightdemo.normalevent

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.ActivityInfo
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.annotation.StringRes
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.Toast
import com.netease.ai.insightdemo.InsightMessageManagerDispatch
import com.netease.ai.insightdemo.R
import com.netease.ai.insightdemo.YourUtil
import com.netease.insightar.InsightConstants
import com.netease.insightar.NEArInsight
import com.netease.insightar.callback.AbsArInsightDataCallback
import com.netease.insightar.callback.AbsArInsightDownloadAndDecompress
import com.netease.insightar.callback.OnArInsightNetworkDataObtainCallback
import com.netease.insightar.callback.OnArInsightResultListener
import com.netease.insightar.entity.ArInsightAlgResult
import com.netease.insightar.entity.ArInsightEventResult
import com.netease.insightar.entity.ArInsightRenderResult
import com.netease.insightar.entity.message.*
import kotlinx.android.synthetic.main.activity_demo_normal_event3.*

/**
 * @author zfxu
 * 通过NEArView以及NEArInsight类来实现普通事件的AR功能 kotlin代码实现
 */
class DemoNormalEventActivityKotlin : AppCompatActivity() {

    companion object {

        const val TAG = "DemoNormalEventActivity3"
        const val REQUEST_CODE_PERMISSION = 101

        fun start(context: Context, eventID: String) {
            val intent = Intent(context, DemoNormalEventActivityKotlin::class.java)
            intent.putExtra(InsightConstants.KEY_EVENT_ID, eventID)
            context.startActivity(intent)
        }

        /**
         *
         * @param context 上下文
         * @param eventID 事件ID
         * @param isOnLineResource 是本地路径或者在线路径
         * @param localPath 本地路径，如果不为空，则直接加载本地路径
         * @param downloadEventOnAll 是否在任何环境下下载事件，默认是true，设置为false会导致在移动网络下弹框
         */
        fun start(context: Context, eventID: String, isOnLineResource: Boolean,
                  localPath: String, downloadEventOnAll: Boolean) {
            val intent = Intent(context, DemoNormalEventActivityKotlin::class.java)
            intent.putExtra(InsightConstants.KEY_IS_ONLINE_RESOURCE, isOnLineResource)
            intent.putExtra(InsightConstants.KEY_EVENT_ID, eventID)
            intent.putExtra(InsightConstants.KEY_LOCAL_PATH, localPath)
            intent.putExtra(InsightConstants.KEY_DOWNLOAD_EVENT_ON_ALL, downloadEventOnAll)
            context.startActivity(intent)
        }
    }


    var mEventID: String = ""
    var mLocalPath: String = ""
    private var mIsDownloadAtAll = true
    private var mIsOnLineResource = true

    var mInsightMessageManagerDispatch: InsightMessageManagerDispatch? = null

    private var mCurrentEventResult: ArInsightEventResult? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_demo_normal_event3)

        //外部传入的事件ID
        val eventID = intent.getStringExtra(InsightConstants.KEY_EVENT_ID)
        val isOnLineResource = intent.getBooleanExtra(InsightConstants.KEY_IS_ONLINE_RESOURCE, true)
        val localPath = intent.getStringExtra(InsightConstants.KEY_LOCAL_PATH)
        val downloadEventOnAll = intent.getBooleanExtra(InsightConstants.KEY_DOWNLOAD_EVENT_ON_ALL, true)

        if (eventID != null) {
            mEventID = eventID
        }
        if (localPath != null) {
            mLocalPath = localPath
        }
        mIsDownloadAtAll = downloadEventOnAll
        mIsOnLineResource = isOnLineResource

        if (TextUtils.isEmpty(mEventID)) {
            showToast("普通事件必须携带PID...")
            return
        }

        mInsightMessageManagerDispatch = InsightMessageManagerDispatch(this@DemoNormalEventActivityKotlin)

        val isCameraGranted = isPermissionGranted(this, Manifest.permission.CAMERA)
        val isWriteGranted = isPermissionGranted(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
        val isAudioRecodeGranted = isPermissionGranted(this,
                Manifest.permission.RECORD_AUDIO)
        val permissionGranted: Boolean
        if (isCameraGranted && isWriteGranted && isAudioRecodeGranted) {
            permissionGranted = true
            Log.i(TAG, "onCreate CAMERA & WRITE permission is granted")
        } else {
            Log.i(TAG, "onCreate CAMERA or WRITE  permission is not granted")
            ActivityCompat.requestPermissions(this,
                    arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA,
                            Manifest.permission.RECORD_AUDIO), REQUEST_CODE_PERMISSION)
            permissionGranted = false
        }
        neArView3.create(false)
        neArView3?.registerArInsightVerificationListener {
          if(!it){
            NEArInsight.clearLocalNormalEventData(mEventID) {
              showToast("资源清理成功，请退出当前页面，重新进入ar体验")
              finish()
            }
          }
        }
        if (permissionGranted) {
            loadData()
        }
    }

    private fun loadData() {
        if (mIsOnLineResource) {
            loadNetData(mIsDownloadAtAll)
        } else {
            if (TextUtils.isEmpty(mLocalPath)) {
                showToast("本地路径为空")
                return
            }
            hideWaitView()
            neArView3.doArShowView(mEventID, mLocalPath, false)
        }
    }

    private fun loadNetData(downloadEventOnAll: Boolean) {
        //加载数据
        showWaitView()
        //对于普通事件的加载，NEArInsight提供相关方法
        //该方法不仅会获取网络数据，也会去下载和解压数据
        if (!downloadEventOnAll && !YourUtil.isWifi(this)) {
            //非wifi情况，且外部传入参数需要提示，先去获取数据
            //获取普通数据，不会下载和解压
            NEArInsight.getSingleNormalEventResource(mEventID, mOnNormalEventDataNetWorkCallback)
        } else {
            //获取普通事件数据，sdk会去下载和解压资源
            NEArInsight.loadSingleNormalEventResource(mEventID, mOnNormalEventDataWorkLoadCallback)
        }
    }


    private fun downloadAndUnzipNormalEventData(t: ArInsightEventResult) {
        NEArInsight.downloadAndUnzipNormalEventResource(t, mOnNormalEventDataWorkDownloadAndUnzipCallback)
    }

    private var mOnNormalEventDataWorkDownloadAndUnzipCallback = object : AbsArInsightDownloadAndDecompress {
        override fun onDownloadStart(id: String?) {
        }

        override fun onDownloadProgress(id: String?, progress: Int) {
            txt_progress.text = "Loading ($progress%)"
            pb_download.progress = progress
        }

        override fun onDownloadError(id: String?, code: Int, msg: String?) {
            hideWaitView()
            showToast("下载失败")
        }

        override fun onDownloadPause(id: String?) {
        }

        override fun onDownloadResume(id: String?) {
        }

        override fun onDownloadSucc(id: String?) {
        }

        override fun unZipStart(id: String?) {
        }

        override fun unZipEnd(id: String?, destFilePath: String?) {
            hideWaitView()
            if (destFilePath != null) {
                neArView3.doArShowView(id, destFilePath, false)
            } else {
                showToast("解压失败")
            }
        }
    }

    var mOnNormalEventDataNetWorkCallback = object : OnArInsightNetworkDataObtainCallback<ArInsightEventResult> {
        override fun onNetworkDataSucc(t: ArInsightEventResult?) {
            if (t == null) {
                showToast("数据为空")
                return
            }
            mCurrentEventResult = t

            //资源数据由于有横竖屏的概念，所以需要根据数据返回的横竖屏的值设置横竖屏
            if (ArInsightEventResult.ScreenOrientation.PORTRAIT == t.eventOrientation) {
                requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
            } else {
                requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
            }

            //可以再去下载和解压数据
            downloadAndUnzipNormalEventData(t)
        }

        override fun onNetworkDataError(code: Int, msg: String?) {
            showToast(msg!!)
        }
    }

    var mOnNormalEventDataWorkLoadCallback = object : AbsArInsightDataCallback<ArInsightEventResult> {
        override fun onDownloadError(id: String?, code: Int, msg: String?) {
            showToast("下载失败")
        }

        override fun onDownloadPause(id: String?) {
        }

        override fun onNetworkDataError(code: Int, msg: String?) {
            showToast(msg!!)
        }

        override fun unZipStart(id: String?) {
        }

        override fun onDownloadStart(id: String?) {
        }

        override fun onDownloadResume(id: String?) {
        }

        override fun onDownloadSucc(id: String?) {
        }

        override fun onNetworkDataSucc(t: ArInsightEventResult?) {
            if (t == null) {
                showToast("数据为空")
                return
            }
            mCurrentEventResult = t

            //资源数据由于有横竖屏的概念，所以需要根据数据返回的横竖屏的值设置横竖屏
            if (ArInsightEventResult.ScreenOrientation.PORTRAIT == t.eventOrientation) {
                requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
            } else {
                requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
            }

        }

        override fun unZipEnd(id: String?, destFilePath: String?) {
            hideWaitView()
            if (destFilePath != null) {
                neArView3.doArShowView(id, destFilePath, false)
            } else {
                showToast("解压失败")
            }
        }

        override fun onDownloadProgress(id: String?, progress: Int) {
            txt_progress.text = "Loading (" + progress + "%)"
            pb_download.progress = progress
        }
    }


    private fun showWaitView() {
        waitLayout3.visibility = View.VISIBLE
        pb_download.progress = 0
        txt_progress.text = "数据加载中... "
    }

    private fun hideWaitView() {
        waitLayout3.visibility = View.GONE
    }

    override fun onResume() {
        super.onResume()
        neArView3.resume()
    }

    override fun onPause() {
        super.onPause()
        neArView3.pause()
    }

    override fun onStart() {
        super.onStart()
        neArView3.start()
        neArView3.registerArInsightResultListener(mArResultListener)
    }

    override fun onStop() {
        super.onStop()
        neArView3.stop()
        neArView3.unregisterArInsightResultListener(mArResultListener)
    }

    override fun onDestroy() {
        super.onDestroy()
        neArView3.destroy()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        neArView3.onBackPressed()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>,
                                            grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == REQUEST_CODE_PERMISSION) {
            var isPermissionGranted = true
            var per: String
            for (i in permissions.indices) {
                per = permissions[i]
                if (per == Manifest.permission.WRITE_EXTERNAL_STORAGE || per == Manifest.permission.CAMERA || per == Manifest.permission.RECORD_AUDIO) {
                    if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                        isPermissionGranted = false
                        break
                    }
                }
            }
            if (!isPermissionGranted) {
                showToast("你已禁止权限...", true)
                finish()
                return
            }
            neArView3.onRequestPermissionsResult(false)
            loadData()
        }
    }

    val mArResultListener = object : OnArInsightResultListener {
        override fun onArRenderResult(p0: ArInsightRenderResult?) {
            // 返回渲染引擎加载资源的状态
        }

        override fun on3dEventMessage(message: IAr3dEventMessage?) {
            val type = message!!.type()
            when (type) {
                IAr3dEventMessage.TYPE_EXECUTE_SCRIPTS -> mInsightMessageManagerDispatch!!.executeScripts(message as ExecuteScript3dEventMessage, neArView3)
                IAr3dEventMessage.TYPE_SHARE -> mInsightMessageManagerDispatch!!.share(message as Share3dEventMessage, mEventID)
                IAr3dEventMessage.TYPE_RELOAD_AR -> mInsightMessageManagerDispatch!!.reloadAr(neArView3
                        , message as Reload3dEventMessage, null)
                IAr3dEventMessage.TYPE_SCREENSHOT -> mInsightMessageManagerDispatch!!.screenShot(neArView3, mEventID)
                IAr3dEventMessage.TYPE_CLOSE_AR -> mInsightMessageManagerDispatch!!.closeAr()
                IAr3dEventMessage.TYPE_OPEN_URL -> mInsightMessageManagerDispatch!!.openUrl(message as OpenUrl3dEventMessage)
                else -> otherEventMessage(message as Common3dEventMessage)
            }
        }

        override fun onArEngineResult(result: ArInsightAlgResult?) {
        }
    }

    private fun otherEventMessage(common3dEventMessage: Common3dEventMessage) {

    }


    protected fun showToast(str: String) {
        showToast(str, true)
    }

    protected fun showToast(str: String, isShort: Boolean) {
        Toast.makeText(this@DemoNormalEventActivityKotlin, str, if (isShort) Toast.LENGTH_SHORT else Toast.LENGTH_LONG).show()
    }

    protected fun showToast(@StringRes str: Int, isShort: Boolean) {
        Toast.makeText(this@DemoNormalEventActivityKotlin, str, if (isShort) Toast.LENGTH_SHORT else Toast.LENGTH_LONG).show()
    }

    private fun isPermissionGranted(context: Context, permission: String): Boolean {
        return ContextCompat.checkSelfPermission(context,
                permission) == PackageManager.PERMISSION_GRANTED
    }
}
