package com.netease.ai.insightdemo.normalevent;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.netease.ai.insightdemo.InsightMessageManagerDispatch;
import com.netease.ai.insightdemo.R;
import com.netease.ai.insightdemo.YourUtil;
import com.netease.insightar.NEArInsight;
import com.netease.insightar.NEArView;
import com.netease.insightar.c.b.k.a;
import com.netease.insightar.callback.AbsArInsightDataCallback;
import com.netease.insightar.callback.AbsArInsightDownloadAndDecompress;
import com.netease.insightar.callback.OnArInsightNetworkDataObtainCallback;
import com.netease.insightar.callback.OnArInsightResultListener;
import com.netease.insightar.callback.OnArInsightVerificationListener;
import com.netease.insightar.entity.ArInsightAlgResult;
import com.netease.insightar.entity.ArInsightEventResult;
import com.netease.insightar.entity.ArInsightRenderResult;
import com.netease.insightar.entity.message.ExecuteScript3dEventMessage;
import com.netease.insightar.entity.message.IAr3dEventMessage;
import com.netease.insightar.entity.message.OpenUrl3dEventMessage;
import com.netease.insightar.entity.message.Reload3dEventMessage;
import com.netease.insightar.entity.message.Share3dEventMessage;
import com.netease.insightar.exception.ArResourceNotFoundException;

import static com.netease.insightar.InsightConstants.KEY_EVENT_ID;
import static com.netease.insightar.entity.message.IAr3dEventMessage.TYPE_CLOSE_AR;
import static com.netease.insightar.entity.message.IAr3dEventMessage.TYPE_EXECUTE_SCRIPTS;
import static com.netease.insightar.entity.message.IAr3dEventMessage.TYPE_OPEN_URL;
import static com.netease.insightar.entity.message.IAr3dEventMessage.TYPE_RELOAD_AR;
import static com.netease.insightar.entity.message.IAr3dEventMessage.TYPE_SCREENSHOT;
import static com.netease.insightar.entity.message.IAr3dEventMessage.TYPE_SHARE;

/**
 * 普通事件通过NeArView及NeArinsihgt来完成集成，java代码实现
 *
 * @author zfxu
 */
public class DemoNormalEventActivityJava extends AppCompatActivity
        implements OnArInsightResultListener, OnArInsightVerificationListener {

    public static void start(Context context, String eventID) {
        Intent intent = new Intent(context, DemoNormalEventActivityJava.class);
        intent.putExtra(KEY_EVENT_ID, eventID);
        context.startActivity(intent);
    }

    private static final int REQUEST_CODE_PERMISSION = 101;
    private static final String TAG = DemoNormalEventActivityJava.class.getSimpleName();
    private String mEventId;

    private NEArView mInsightArView;
    private TextView mLoadingView;
    private InsightMessageManagerDispatch mInsightMessageManagerDispatch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demo_normal_event4);

        initView();

        mEventId = getIntent().getStringExtra(KEY_EVENT_ID);

        if (TextUtils.isEmpty(mEventId)) {
            showToast("普通事件必须携带pid...");
            finish();
        }

        boolean isCameraGranted = isPermissionGranted(this, Manifest.permission.CAMERA);
        boolean isWriteGranted =
                isPermissionGranted(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        boolean isAudioRecodeGranted = isPermissionGranted(this, Manifest.permission.RECORD_AUDIO);
        boolean permissionGranted;
        if (isCameraGranted && isWriteGranted && isAudioRecodeGranted) {
            permissionGranted = true;
        } else {
            ActivityCompat.requestPermissions(this, new String[]{
                    Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA,
                    Manifest.permission.RECORD_AUDIO
            }, REQUEST_CODE_PERMISSION);
            permissionGranted = false;
        }

        mInsightMessageManagerDispatch = new InsightMessageManagerDispatch(this);
        mInsightArView.create(false);

        if (permissionGranted) {
            loadData();
        }
    }

    private void initView() {
        mInsightArView = findViewById(R.id.neArView);
        mLoadingView = findViewById(R.id.waitTextView);
        mInsightArView.registerArInsightVerificationListener(this);
    }

    private void showWaitView() {
        mLoadingView.setVisibility(View.VISIBLE);
    }

    private void hideWaitView() {
        mLoadingView.setVisibility(View.GONE);
    }

    private void loadData() {
        if (!YourUtil.isNetworkAvailable(this)) {
            showToast("当前无网络...");
            return;
        }

        showWaitView();

        if (!YourUtil.isWifi(this)) {
            //移动网络下，不去下载和解压数据，仅仅是去get
            NEArInsight.getSingleNormalEventResource(mEventId,
                    new OnArInsightNetworkDataObtainCallback<ArInsightEventResult>() {
                        @Override
                        public void onNetworkDataSucc(
                                @Nullable final ArInsightEventResult arInsightEventResult) {
                            if (arInsightEventResult == null) {
                                showToast("数据为空...");
                                return;
                            }

                            //设置横竖屏
                            setRequestedOrientation(arInsightEventResult);

                            //如果服务端数据未改变，而且前面已经下载，可以直接显示
                            if (!TextUtils.isEmpty(arInsightEventResult.getLocalResourcePath())) {
                                hideWaitView();
                                try {
                                    mInsightArView.doArShowView(arInsightEventResult.getEventId(),
                                            arInsightEventResult.getLocalResourcePath(), false);
                                } catch (ArResourceNotFoundException e) {
                                    showToast("AR显示异常");
                                    //e.printStackTrace();
                                }
                                return;
                            }

                            //这里外部可以显示弹框，让用户选择下载或者取消
                            final AlertDialog.Builder normalDialog =
                                    new AlertDialog.Builder(DemoNormalEventActivityJava.this);
                            normalDialog.setIcon(R.drawable.insight_ar_app_icon);
                            normalDialog.setTitle("提示");
                            normalDialog.setMessage(
                                    "下载讲消耗: " + arInsightEventResult.getEventSize() + " 流量");
                            normalDialog.setPositiveButton("确定", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    downloadNormalEventData(arInsightEventResult);
                                }
                            });
                            normalDialog.setNegativeButton("取消", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    //Todo
                                }
                            });
                            normalDialog.show();
                        }

                        @Override
                        public void onNetworkDataError(int code, String msg) {
                            hideWaitView();
                            showToast(msg);
                        }
                    });
        } else {
            NEArInsight.loadSingleNormalEventResource(mEventId,
                    new AbsArInsightDataCallback<ArInsightEventResult>() {
                        @Override
                        public void onDownloadStart(String id) {

                        }

                        @Override
                        public void onDownloadProgress(String id, int progress) {
                            Log.i(TAG, "loadSingleNormalEventResource progress: " + progress);
                        }

                        @Override
                        public void onDownloadError(String id, int code, String msg) {
                            hideWaitView();
                            showToast("下载失败");
                        }

                        @Override
                        public void onDownloadPause(String id) {

                        }

                        @Override
                        public void onDownloadResume(String id) {

                        }

                        @Override
                        public void onDownloadSucc(String id) {

                        }

                        @Override
                        public void onNetworkDataSucc(
                                @Nullable ArInsightEventResult arInsightEventResult) {
                            if (arInsightEventResult == null) {
                                hideWaitView();
                                showToast("数据为空");
                                return;
                            }
                            setRequestedOrientation(arInsightEventResult);
                        }

                        @Override
                        public void onNetworkDataError(int code, String msg) {
                            hideWaitView();
                            showToast(msg);
                        }

                        @Override
                        public void unZipStart(String id) {

                        }

                        @Override
                        public void unZipEnd(String id, @Nullable String destFilePath) {
                            hideWaitView();
                            if (TextUtils.isEmpty(destFilePath)) {
                                showToast("解压失败...");
                                return;
                            }
                            try {
                                mInsightArView.doArShowView(id, destFilePath, false);
                            } catch (ArResourceNotFoundException e) {
                                e.printStackTrace();
                                showToast("AR数据异常...");
                            }
                        }
                    });
        }
    }

    @Override public void onVerificationResult(boolean b) {
        if (!b){//资源校验通不过(出现这种几率很低)，删除本地的资源，提示用户重新下载资源
            NEArInsight.clearLocalNormalEventData(mEventId, new a() {
                @Override public void a(boolean result) {
                    if (result){//清理成功
                        showToast("资源清理成功，请退出当前页面，重新进入ar体验");
                        finish();
                    }
                }
            });
        }
    }

    private void downloadNormalEventData(ArInsightEventResult arInsightEventResult) {
        showWaitView();
        NEArInsight.downloadAndUnzipNormalEventResource(arInsightEventResult,
                new AbsArInsightDownloadAndDecompress() {
                    @Override
                    public void onDownloadStart(String id) {

                    }

                    @Override
                    public void onDownloadProgress(String id, int progress) {
                        Log.i(TAG, "downloadAndUnzipNormalEventResource: progress ---- " + progress);
                    }

                    @Override
                    public void onDownloadError(String id, int code, String msg) {
                        hideWaitView();
                        showToast("下载失败");
                    }

                    @Override
                    public void onDownloadPause(String id) {

                    }

                    @Override
                    public void onDownloadResume(String id) {

                    }

                    @Override
                    public void onDownloadSucc(String id) {

                    }

                    @Override
                    public void unZipStart(String id) {

                    }

                    @Override
                    public void unZipEnd(String id, String destFilePath) {
                        hideWaitView();
                        try {
                            mInsightArView.doArShowView(id, destFilePath, false);
                        } catch (ArResourceNotFoundException e) {
                            e.printStackTrace();
                            showToast("AR显示异常");
                        }
                    }
                });
    }

    private void setRequestedOrientation(ArInsightEventResult arInsightEventResult) {
        if (arInsightEventResult.getEventOrientation()
                == ArInsightEventResult.ScreenOrientation.PORTRAIT) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        mInsightArView.stop();
        mInsightArView.unregisterArInsightResultListener(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mInsightArView.start();
        mInsightArView.registerArInsightResultListener(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mInsightArView.destroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mInsightArView.pause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mInsightArView.resume();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        mInsightArView.onBackPressed();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_CODE_PERMISSION) {
            boolean isPermissionGranted = true;
            String per;
            for (int i = 0; i < permissions.length; i++) {
                per = permissions[i];
                if (per.equals(Manifest.permission.WRITE_EXTERNAL_STORAGE) || per.equals(
                        Manifest.permission.CAMERA) || per.equals(Manifest.permission.RECORD_AUDIO)) {
                    if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                        isPermissionGranted = false;
                        break;
                    }
                }
            }
            if (!isPermissionGranted) {
                showToast("你已禁止权限...", true);
                finish();
                return;
            }
            /**
             * 注意：onRequestPermissionsResult(boolean)
             * 布尔参数是代表当前加载的资源是否是云识别模式,而非权限回调结果
             * true代表云识别模式，false:非云识别
             */
            mInsightArView.onRequestPermissionsResult(false);
            loadData();
        }
    }

    protected void showToast(String str) {
        showToast(str, true);
    }

    protected void showToast(String str, boolean isShort) {
        Toast.makeText(this, str, isShort ? Toast.LENGTH_SHORT : Toast.LENGTH_LONG).show();
    }

    protected void showToast(@StringRes int str, boolean isShort) {
        Toast.makeText(this, str, isShort ? Toast.LENGTH_SHORT : Toast.LENGTH_LONG).show();
    }

    boolean isPermissionGranted(Context context, String permission) {
        return ContextCompat.checkSelfPermission(context, permission)
                == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void on3dEventMessage(IAr3dEventMessage message) {
        int type = message.type();
        switch (type) {
            case TYPE_RELOAD_AR:
                mInsightMessageManagerDispatch.reloadAr(mInsightArView,
                        (Reload3dEventMessage) message, null);
                break;
            case TYPE_EXECUTE_SCRIPTS:
                mInsightMessageManagerDispatch.executeScripts((ExecuteScript3dEventMessage) message,
                        mInsightArView);
                break;
            case TYPE_SHARE:
                mInsightMessageManagerDispatch.share((Share3dEventMessage) message,
                        mEventId);
                break;
            case TYPE_SCREENSHOT:
                mInsightMessageManagerDispatch.screenShot(mInsightArView, mEventId);
                break;
            case TYPE_CLOSE_AR:
                mInsightMessageManagerDispatch.closeAr();
                break;
            case TYPE_OPEN_URL:
                mInsightMessageManagerDispatch.openUrl((OpenUrl3dEventMessage) message);
                break;
            default:
                //TODO
                break;
        }
    }

    @Override
    public void onArEngineResult(ArInsightAlgResult result) {

    }

    @Override
    public void onArRenderResult(ArInsightRenderResult arInsightRenderResult) {

    }
}
