package com.netease.ai.insightdemo.cloudreco;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.netease.ai.insightdemo.InsightMessageManagerDispatch;
import com.netease.ai.insightdemo.R;
import com.netease.insightar.NEArInsight;
import com.netease.insightar.NEArView;
import com.netease.insightar.callback.AbsArInsightDataCallback;
import com.netease.insightar.callback.OnArInsightResultListener;
import com.netease.insightar.entity.ArInsightAlgResult;
import com.netease.insightar.entity.ArInsightEventResult;
import com.netease.insightar.entity.ArInsightRecoResult;
import com.netease.insightar.entity.ArInsightRenderResult;
import com.netease.insightar.entity.message.ExecuteScript3dEventMessage;
import com.netease.insightar.entity.message.IAr3dEventMessage;
import com.netease.insightar.entity.message.OpenUrl3dEventMessage;
import com.netease.insightar.entity.message.Reload3dEventMessage;
import com.netease.insightar.entity.message.Share3dEventMessage;
import com.netease.insightar.exception.ArResourceNotFoundException;

import static com.netease.insightar.entity.message.IAr3dEventMessage.TYPE_CLOSE_AR;
import static com.netease.insightar.entity.message.IAr3dEventMessage.TYPE_EXECUTE_SCRIPTS;
import static com.netease.insightar.entity.message.IAr3dEventMessage.TYPE_OPEN_URL;
import static com.netease.insightar.entity.message.IAr3dEventMessage.TYPE_RELOAD_AR;
import static com.netease.insightar.entity.message.IAr3dEventMessage.TYPE_SCREENSHOT;
import static com.netease.insightar.entity.message.IAr3dEventMessage.TYPE_SHARE;

/**
 * 通用识别通过NeArView及NeArinsihgt来完成集成，java代码实现
 *
 * @author zfxu
 */
public class DemoCloudRecoActivityJava extends AppCompatActivity implements OnArInsightResultListener {

    private static final String TAG = DemoCloudRecoActivityJava.class.getSimpleName();
    private static final int REQUEST_CODE_PERMISSION = 110;
    private NEArView mInsightArView;
    private TextView mWaitView;
    private InsightMessageManagerDispatch mInsightMessageManagerDispatch;

    private String mEventID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demo_cloud_reco4);
        initView();

        //判断权限
        boolean isCameraGranted = isPermissionGranted(this, Manifest.permission.CAMERA);
        boolean isWriteGranted =
                isPermissionGranted(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        boolean isAudioRecodeGranted = isPermissionGranted(this, Manifest.permission.RECORD_AUDIO);
        boolean granted;
        if (isCameraGranted && isWriteGranted && isAudioRecodeGranted) {
            Log.i(TAG, "onCreate CAMERA & WRITE permission is granted");
            granted = true;
        } else {
            Log.i(TAG, "onCreate CAMERA or WRITE  permission is not granted");
            ActivityCompat.requestPermissions(this, new String[]{
                    Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA,
                    Manifest.permission.RECORD_AUDIO
            }, REQUEST_CODE_PERMISSION);
            granted = false;
        }

        mInsightMessageManagerDispatch = new InsightMessageManagerDispatch(this);
        mInsightArView.create(true);

        if (granted) {
            loadData();
        }
    }

    private void loadData() {
        showWaitView();
        //对于通用识别的加载，NEArInsight提供相关方法
        //该方法不仅会获取网络数据，也会去下载和解压数据
        NEArInsight.loadCloudRecoResource(mLoadCloudResourceCallback);
    }

    private AbsArInsightDataCallback<ArInsightRecoResult> mLoadCloudResourceCallback =
            new AbsArInsightDataCallback<ArInsightRecoResult>() {
                @Override
                public void onDownloadStart(String id) {

                }

                @Override
                public void onDownloadProgress(String id, int progress) {

                }

                @Override
                public void onDownloadError(String id, int code, String msg) {
                    hideWaitView();
                    showToast("通用识别数据下载失败");
                }

                @Override
                public void onDownloadPause(String id) {

                }

                @Override
                public void onDownloadResume(String id) {

                }

                @Override
                public void onDownloadSucc(String id) {

                }

                @Override
                public void onNetworkDataSucc(@Nullable ArInsightRecoResult arInsightRecoResult) {

                }

                @Override
                public void onNetworkDataError(int code, String msg) {
                    hideWaitView();
                    showToast("通用识别数据加载失败");
                }

                @Override
                public void unZipStart(String id) {

                }

                @Override
                public void unZipEnd(String id, String destFilePath) {
                    hideWaitView();
                    try {
                        mInsightArView.doArShowView(null, destFilePath, true);
                    } catch (ArResourceNotFoundException e) {
                        e.printStackTrace();
                        showToast("ar显示异常...");
                    }
                }
            };

    private void showWaitView() {
        mWaitView.setVisibility(View.VISIBLE);
    }

    private void hideWaitView() {
        mWaitView.setVisibility(View.GONE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_CODE_PERMISSION) {
            boolean isPermissionGranted = true;
            String per;
            for (int i = 0; i < permissions.length; i++) {
                per = permissions[i];
                if (per.equals(Manifest.permission.WRITE_EXTERNAL_STORAGE) || per.equals(
                        Manifest.permission.CAMERA) || per.equals(Manifest.permission.RECORD_AUDIO)) {
                    if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                        isPermissionGranted = false;
                        break;
                    }
                }
            }

            if (!isPermissionGranted) {
                showToast("你已禁止权限...", true);
                finish();
                return;
            }

            mInsightArView.onRequestPermissionsResult(true);
            loadData();
        }
    }

    protected void showToast(String str) {
        showToast(str, true);
    }

    protected void showToast(String str, boolean isShort) {
        Toast.makeText(this, str, isShort ? Toast.LENGTH_SHORT : Toast.LENGTH_LONG).show();
    }

    protected void showToast(@StringRes int str, boolean isShort) {
        Toast.makeText(this, str, isShort ? Toast.LENGTH_SHORT : Toast.LENGTH_LONG).show();
    }

    private void initView() {
        mInsightArView = findViewById(R.id.neArView4);
        mWaitView = findViewById(R.id.waitTextView);
    }

    private boolean isPermissionGranted(Context context, String permission) {
        return ContextCompat.checkSelfPermission(context, permission)
                == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    protected void onStart() {
        super.onStart();
        mInsightArView.start();
        mInsightArView.registerArInsightResultListener(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mInsightArView.pause();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mInsightArView.stop();
        mInsightArView.unregisterArInsightResultListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mInsightArView.resume();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        mInsightArView.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mInsightArView.destroy();
    }

    @Override
    public void on3dEventMessage(IAr3dEventMessage message) {
        int type = message.type();
        switch (type) {
            case TYPE_RELOAD_AR:
                mInsightMessageManagerDispatch.reloadAr(mInsightArView,
                        (Reload3dEventMessage) message,
                        new InsightMessageManagerDispatch.ReloadArRunnable() {
                            @Override
                            public void run(String pid) {
                                showWaitView();
                                mEventID = pid;
                                //你也可以调用get方法获取网络数据，然后再去下载，具体见API文档
                                NEArInsight.loadSingleNormalEventResource(mEventID,
                                        mArInsightEventResultCallback);
                            }
                        });
                break;
            case TYPE_EXECUTE_SCRIPTS:
                mInsightMessageManagerDispatch.executeScripts((ExecuteScript3dEventMessage) message,
                        mInsightArView);
                break;
            case TYPE_SHARE:
                mInsightMessageManagerDispatch.share((Share3dEventMessage) message,
                        mEventID);
                break;
            case TYPE_SCREENSHOT:
                mInsightMessageManagerDispatch.screenShot(mInsightArView, mEventID);
                break;
            case TYPE_CLOSE_AR:
                mInsightMessageManagerDispatch.closeAr();
                break;
            case TYPE_OPEN_URL:
                mInsightMessageManagerDispatch.openUrl((OpenUrl3dEventMessage) message);
                break;
            default:
                //TODO
                break;
        }
    }

    private AbsArInsightDataCallback<ArInsightEventResult> mArInsightEventResultCallback =
            new AbsArInsightDataCallback<ArInsightEventResult>() {
                @Override
                public void onDownloadStart(String id) {

                }

                @Override
                public void onDownloadProgress(String id, int progress) {

                }

                @Override
                public void onDownloadError(String id, int code, String msg) {
                    hideWaitView();
                    showToast("事件下载失败");
                }

                @Override
                public void onDownloadPause(String id) {

                }

                @Override
                public void onDownloadResume(String id) {

                }

                @Override
                public void onDownloadSucc(String id) {

                }

                @Override
                public void onNetworkDataSucc(@Nullable ArInsightEventResult arInsightEventResult) {

                }

                @Override
                public void onNetworkDataError(int code, String msg) {
                    hideWaitView();
                    showToast("事件获取失败");
                }

                @Override
                public void unZipStart(String id) {

                }

                @Override
                public void unZipEnd(String id, String destFilePath) {
                    hideWaitView();
                    try {
                        mInsightArView.doArShowView(id, destFilePath, false);
                    } catch (ArResourceNotFoundException e) {
                        e.printStackTrace();
                        showToast("ar显示异常");
                    }
                }
            };

    @Override
    public void onArEngineResult(ArInsightAlgResult result) {

    }

    @Override
    public void onArRenderResult(ArInsightRenderResult arInsightRenderResult) {

    }
}
