package com.netease.ai.insightdemo;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Window;

import java.util.Timer;
import java.util.TimerTask;

/**
 * 闪屏界面
 */

public class SplashActivity extends Activity {

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_splash);
        startTiming();
    }

    private void startTiming() {
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override public void run() {
                goNext();
            }
        }, 2 * 1000);
    }

    private void goNext() {
        Intent intent = new Intent(SplashActivity.this, OutDemoMainActivityJava.class);
        startActivity(intent);
        finish();
    }
}
