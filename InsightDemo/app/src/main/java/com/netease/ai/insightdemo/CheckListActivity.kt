package com.netease.ai.insightdemo

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_check_list.*

class CheckListActivity : AppCompatActivity() {

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_check_list)


    checkListTitle.text = "Hi, 小伙伴: \n\n如果已经完成SDK的接入，请确认以下信息，保证接入是否完整："
    checkListContent.text = "1. 请确认是否实现\non3dEventMessage(IAr3dEventMessage message)\n回调接口，并对7个消息类型都做了相应处理\n\n" +
        "2. 请确认对一些异常状态的回调方法（如请求失败、下载失败等）做了相应的UI操作\n\n" +
        "3. 请确认SDK接口方法是否仅在「使用AR功能」时调用，而不是在其他App功能模块也存在调用\n\n" +
        "4. 请确认一些状态回调的注册（register）及注销（unregister）是否完整"

  }
}
