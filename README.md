### Insight SDK  
  
#### Release Note  

**v2.9.0**
> 请在**master**分支下载（或使用[Tags](https://gitlab.com/InsightAR/InsightARSDK_Android/tags)下载）

> [接入参考文档](https://near.yuque.com/docs/share/2398448a-59af-4717-9035-ccff1e9a053f)   
> [详细API参考文档](https://near.yuque.com/docs/share/2b4e012a-df65-420e-b7ed-a84cd82591eb)  
> [FAQ](https://near.yuque.com/docs/share/57e2cfbf-d05c-4975-99fe-4eaaf11cd168)

* 优化深度学习依赖库mnn,由1.0升级为2.0
* 适配mnn升级，原物体检测，物体跟踪，手势检测，天空检测，人体姿态识别均有适配更新
* 新增04041场景，用于图片/物体检测场景


**v2.8.1**
> 请在**master**分支下载（或使用[Tags](https://gitlab.com/InsightAR/InsightARSDK_Android/tags)下载）

> [接入参考文档](https://near.yuque.com/docs/share/2398448a-59af-4717-9035-ccff1e9a053f)   
> [详细API参考文档](https://near.yuque.com/docs/share/2b4e012a-df65-420e-b7ed-a84cd82591eb)  
> [FAQ](https://near.yuque.com/docs/share/57e2cfbf-d05c-4975-99fe-4eaaf11cd168)

* 算法支持targetSdkVersion升级到31
* 渲染引擎增加了本地文件加载图片逻辑
* 新增资源校验出错的回调，以便接入方重新下载资源

**v2.8.0**
> 请在**master**分支下载（或使用[Tags](https://gitlab.com/InsightAR/InsightARSDK_Android/tags)下载）

> [接入参考文档](https://near.yuque.com/docs/share/2398448a-59af-4717-9035-ccff1e9a053f)   
> [详细API参考文档](https://near.yuque.com/docs/share/2b4e012a-df65-420e-b7ed-a84cd82591eb)  
> [FAQ](https://near.yuque.com/docs/share/57e2cfbf-d05c-4975-99fe-4eaaf11cd168)

* 渲染引擎修复了protocol buffer对递归深度限制
* 修复了lua层字符串引号转义问题
* 渲染引擎音视频ARScript新增接口支持从本地路径设置资源片段，Animator支持获取动画状态名等
* 修复nova 8 pro等手机录屏失败问题


**v2.7.1_hotfix**
> 请在**master**分支下载（或使用[Tags](https://gitlab.com/InsightAR/InsightARSDK_Android/tags)下载）

> [接入参考文档](https://near.yuque.com/docs/share/2398448a-59af-4717-9035-ccff1e9a053f)   
> [详细API参考文档](https://near.yuque.com/docs/share/2b4e012a-df65-420e-b7ed-a84cd82591eb)  
> [FAQ](https://near.yuque.com/docs/share/57e2cfbf-d05c-4975-99fe-4eaaf11cd168)

* 修复marker叠加semiVIO不可用问题

**v2.7.1**
> 请在**master**分支下载（或使用[Tags](https://gitlab.com/InsightAR/InsightARSDK_Android/tags)下载）

> [接入参考文档](https://near.yuque.com/docs/share/2398448a-59af-4717-9035-ccff1e9a053f)   
> [详细API参考文档](https://near.yuque.com/docs/share/2b4e012a-df65-420e-b7ed-a84cd82591eb)  
> [FAQ](https://near.yuque.com/docs/share/57e2cfbf-d05c-4975-99fe-4eaaf11cd168)

* 修复华为arengine和arcore退出卡死问题
* 修复引擎加载png图片丢失alpha通道问题
* 修复引擎加载不同图片地址返回相同图片的问题

**v2.7.0**
> 请在**master**分支下载（或使用[Tags](https://gitlab.com/InsightAR/InsightARSDK_Android/tags)下载）

> [接入参考文档](https://near.yuque.com/docs/share/2398448a-59af-4717-9035-ccff1e9a053f)   
> [详细API参考文档](https://near.yuque.com/docs/share/2b4e012a-df65-420e-b7ed-a84cd82591eb)  
> [FAQ](https://near.yuque.com/docs/share/57e2cfbf-d05c-4975-99fe-4eaaf11cd168)

* 修复渲染引擎在小米12等机型上crash问题

**v2.6.2**
> 请在**master**分支下载（或使用[Tags](https://gitlab.com/InsightAR/InsightARSDK_Android/tags)下载）

> [接入参考文档](https://near.yuque.com/docs/share/2398448a-59af-4717-9035-ccff1e9a053f)   
> [详细API参考文档](https://near.yuque.com/docs/share/2b4e012a-df65-420e-b7ed-a84cd82591eb)  
> [FAQ](https://near.yuque.com/docs/share/57e2cfbf-d05c-4975-99fe-4eaaf11cd168)

* 算法兼容支持google arcore 1.29和华为arengine 3.5.0.4版本
* 由于google arcore cpu架构特性原因，从这个版本开始不再支持armeabi
* 一些bugfix等

> 包体大小说明：  
> 静态包32位（含so）大小:约6.2M。V2签名下集成后apk体积增加5.8M左右  
静态包64位（含so）大小:约12.6M。V2签名下集成后apk体积增加12M左右  
动态包（32bit _dl包下需额外下载12M so）大小:961KB。V2签名下集成后apk体积增加0.6M

**v2.6.1**  
> 请在**master**分支下载（或使用[Tags](https://gitlab.com/InsightAR/InsightARSDK_Android/tags)下载）

> [接入参考文档](https://near.yuque.com/docs/share/2398448a-59af-4717-9035-ccff1e9a053f)   
> [详细API参考文档](https://near.yuque.com/docs/share/2b4e012a-df65-420e-b7ed-a84cd82591eb)  
> [FAQ](https://near.yuque.com/docs/share/57e2cfbf-d05c-4975-99fe-4eaaf11cd168)

* 修复录屏关闭权限，没有回调错误结果
* 修复引擎下载图片会偶发性crash问题

> 包体大小说明：  
> 静态包32位（含so）大小:约6.2M。V2签名下集成后apk体积增加5.8M左右  
静态包64位（含so）大小:约12.6M。V2签名下集成后apk体积增加12M左右  
动态包（32bit _dl包下需额外下载12M so）大小:961KB。V2签名下集成后apk体积增加0.6M


**v2.6.0**  
> 请在**master**分支下载（或使用[Tags](https://gitlab.com/InsightAR/InsightARSDK_Android/tags)下载）

> [接入参考文档](https://near.yuque.com/docs/share/2398448a-59af-4717-9035-ccff1e9a053f)   
> [详细API参考文档](https://near.yuque.com/docs/share/2b4e012a-df65-420e-b7ed-a84cd82591eb)  
> [FAQ](https://near.yuque.com/docs/share/57e2cfbf-d05c-4975-99fe-4eaaf11cd168)

* 通用网络请求优化,支持调用方传入编解码参数
* 渲染引擎优化动画生命周期，跟随物体显隐状态自动重置动画

> 包体大小说明：  
> 静态包32位（含so）大小:约6.2M。V2签名下集成后apk体积增加5.8M左右  
静态包64位（含so）大小:约12.6M。V2签名下集成后apk体积增加12M左右  
动态包（32bit _dl包下需额外下载12M so）大小:961KB。V2签名下集成后apk体积增加0.6M

**v2.5.1_hotfix**  
> 请在**master**分支下载（或使用[Tags](https://gitlab.com/InsightAR/InsightARSDK_Android/tags)下载）

> [接入参考文档](https://near.yuque.com/docs/share/2398448a-59af-4717-9035-ccff1e9a053f)   
> [详细API参考文档](https://near.yuque.com/docs/share/2b4e012a-df65-420e-b7ed-a84cd82591eb)  
> [FAQ](https://near.yuque.com/docs/share/57e2cfbf-d05c-4975-99fe-4eaaf11cd168)

* 修复部分手机上无法生成天空分割效果的问题

> 包体大小说明：  
> 静态包32位（含so）大小:约6.3M。V2签名下集成后apk体积增加5.8M左右  
静态包64位（含so）大小:约12.6M。V2签名下集成后apk体积增加12M左右  
动态包（32bit _dl包下需额外下载12M so）大小:961KB。V2签名下集成后apk体积增加0.6M

**v2.5.1**  
> 请在**master**分支下载（或使用[Tags](https://gitlab.com/InsightAR/InsightARSDK_Android/tags)下载）

> [接入参考文档](https://near.yuque.com/docs/share/2398448a-59af-4717-9035-ccff1e9a053f)   
> [详细API参考文档](https://near.yuque.com/docs/share/2b4e012a-df65-420e-b7ed-a84cd82591eb)  
> [FAQ](https://near.yuque.com/docs/share/57e2cfbf-d05c-4975-99fe-4eaaf11cd168)

* 修复OPPO R11/魅族 M6 Note/红米 5Plus反复退出进入后偶现黑屏问题；  
* 修复低端手机上偶现几帧花屏问题；
*  修复偶现极低概率crash问题

> 包体大小说明：  
> 静态包32位（含so）大小:约6.3M。V2签名下集成后apk体积增加5.8M左右  
静态包64位（含so）大小:约12.6M。V2签名下集成后apk体积增加12M左右  
动态包（32bit _dl包下需额外下载12M so）大小:961KB。V2签名下集成后apk体积增加0.6M

**v2.5.0**  
> 请在**master**分支下载（或使用[Tags](https://gitlab.com/InsightAR/InsightARSDK_Android/tags)下载）

> [接入参考文档](https://near.yuque.com/docs/share/2398448a-59af-4717-9035-ccff1e9a053f)   
> [详细API参考文档](https://near.yuque.com/docs/share/2b4e012a-df65-420e-b7ed-a84cd82591eb)  
> [FAQ](https://near.yuque.com/docs/share/57e2cfbf-d05c-4975-99fe-4eaaf11cd168)

* 新增天空分割算法支持；  
* 修复一些微小的Bug

> 包体大小说明：  
> 静态包32位（含so）大小:约6.3M。V2签名下集成后apk体积增加5.8M左右  
静态包64位（含so）大小:约12.6M。V2签名下集成后apk体积增加12M左右  
动态包（32bit _dl包下需额外下载12M so）大小:961KB。V2签名下集成后apk体积增加0.6M

**v2.4.1**  
> 请在**master**分支下载（或使用[Tags](https://gitlab.com/InsightAR/InsightARSDK_Android/tags)下载）

> [接入参考文档](https://near.yuque.com/docs/share/2398448a-59af-4717-9035-ccff1e9a053f)   
> [详细API参考文档](https://near.yuque.com/docs/share/2b4e012a-df65-420e-b7ed-a84cd82591eb)  
> [FAQ](https://near.yuque.com/docs/share/57e2cfbf-d05c-4975-99fe-4eaaf11cd168)

* 支持外部数据传入LUA解析逻辑；  
* 修复多个音频（数十个以上）切换播放时引起状态机异常播放失败的问题  
* 渲染引擎修复一些AR内容制作上的Bug

> 包体大小说明：  
> 静态包32位（含so）大小:约6.1M。V2签名下集成后apk体积增加5.8M  
静态包64位（含so）大小:约12.4M。V2签名下集成后apk体积增加12M  
动态包（32bit _dl包下需额外下载11.7M so）大小:961KB。V2签名下集成后apk体积增加0.6M

**v2.4.0**  
> 请移步**release**分支下载（或使用[Tags](https://gitlab.com/InsightAR/InsightARSDK_Android/tags)下载）

> [接入参考文档](https://near.yuque.com/docs/share/2398448a-59af-4717-9035-ccff1e9a053f)   
> [详细API参考文档](https://near.yuque.com/docs/share/2b4e012a-df65-420e-b7ed-a84cd82591eb)  
> [FAQ](https://near.yuque.com/docs/share/57e2cfbf-d05c-4975-99fe-4eaaf11cd168)

* 支持扫描ARCode跳转H5逻辑；  
* 修复多人AR在服务端证书过期或不存在时引起的Crash问题  
* 去掉获取用户网络相关数据的埋点代码
*  恢复下载资源包时的证书校验
*  渲染引擎增加内容表现接口扩展

> 包体大小说明：  
> 静态包32位（含so）大小:约6.1M。V2签名下集成后apk体积增加5.8M  
静态包64位（含so）大小:约12.3M。V2签名下集成后apk体积增加12M  
动态包（32bit _dl包下需额外下载11.7M so）大小:960KB。V2签名下集成后apk体积增加0.6M

**v2.3.4**  
> 请使用[Tags](https://gitlab.com/InsightAR/InsightARSDK_Android/tags)下载

> [接入参考文档](https://near.yuque.com/docs/share/2398448a-59af-4717-9035-ccff1e9a053f)   
> [详细API参考文档](https://near.yuque.com/docs/share/2b4e012a-df65-420e-b7ed-a84cd82591eb)  
> [FAQ](https://near.yuque.com/docs/share/57e2cfbf-d05c-4975-99fe-4eaaf11cd168)

* 修复3D物体跟踪时卡顿问题

> 包体大小说明：  
> 静态包32位（含so）大小:约6.1M。V2签名下集成后apk体积增加5.8M  
静态包64位（含so）大小:约12.3M。V2签名下集成后apk体积增加12M  
动态包（32bit _dl包下需额外下载11.7M so）大小:960KB。V2签名下集成后apk体积增加0.6M

**v2.3.3**  
> 请移步**release**分支下载（或使用[Tags](https://gitlab.com/InsightAR/InsightARSDK_Android/tags)下载）

> [接入参考文档](https://near.yuque.com/docs/share/2398448a-59af-4717-9035-ccff1e9a053f)   
> [详细API参考文档](https://near.yuque.com/docs/share/2b4e012a-df65-420e-b7ed-a84cd82591eb)  
> [FAQ](https://near.yuque.com/docs/share/57e2cfbf-d05c-4975-99fe-4eaaf11cd168)

* 提升SDK最低支持Android系统版本至「Android 6.0」  
* 修复在个别低端机上会偶现的ANR异常
*  修复个别手机IMU数据未经校准引起的模型Drift问题

> 包体大小说明：  
> 静态包32位（含so）大小:约6.1M。V2签名下集成后apk体积增加5.8M  
静态包64位（含so）大小:约12.3M。V2签名下集成后apk体积增加12M  
动态包（32bit _dl包下需额外下载11.7M so）大小:960KB。V2签名下集成后apk体积增加0.6M

**v2.3.1**  
> 请移步**release**分支下载（或使用[Tags](https://gitlab.com/InsightAR/InsightARSDK_Android/tags)下载）

> [接入参考文档](https://near.yuque.com/docs/share/2398448a-59af-4717-9035-ccff1e9a053f)   
> [详细API参考文档](https://near.yuque.com/docs/share/2b4e012a-df65-420e-b7ed-a84cd82591eb)  
> [FAQ](https://near.yuque.com/docs/share/57e2cfbf-d05c-4975-99fe-4eaaf11cd168)

* 优化洞见Logo水印大小的适配  
* 修复个别内容表现问题
* 优化引擎渲染效率，解决部分内容卡顿问题

> 包体大小说明：  
> 静态包32位（含so）大小:约6.1M。V2签名下集成后apk体积增加5.8M  
静态包64位（含so）大小:约12.3M。V2签名下集成后apk体积增加12M  
动态包（32bit _dl包下需额外下载11.7M so）大小:960KB。V2签名下集成后apk体积增加0.6M

**v2.3.0**  
> 请移步**release**分支下载（或使用[Tags](https://gitlab.com/InsightAR/InsightARSDK_Android/tags)下载）

> [接入参考文档](https://near.yuque.com/docs/share/2398448a-59af-4717-9035-ccff1e9a053f)   
> [详细API参考文档](https://near.yuque.com/docs/share/2b4e012a-df65-420e-b7ed-a84cd82591eb)  
> [FAQ](https://near.yuque.com/docs/share/57e2cfbf-d05c-4975-99fe-4eaaf11cd168)

* 增加多人AR证书更新机制  
* 新增对HUAWEI AR Engine SDK以及ARCore的支持  
* 增加算法稳定性  
* 修复若干内存泄露问题 
* 渲染恢复对物理引擎的支持

> 包体大小说明：  
> 静态包32位（含so）大小:约6.1M。V2签名下集成后apk体积增加5.8M  
静态包64位（含so）大小:约12.3M。V2签名下集成后apk体积增加12M  
动态包（32bit _dl包下需额外下载11.7M so）大小:960KB。V2签名下集成后apk体积增加0.6M 

**v2.2.0**  
> 请移步**release**分支下载（或使用[Tags](https://gitlab.com/InsightAR/InsightARSDK_Android/tags)下载）

> [接入参考文档](https://near.yuque.com/docs/share/2398448a-59af-4717-9035-ccff1e9a053f)   
> [详细API参考文档](https://near.yuque.com/docs/share/2b4e012a-df65-420e-b7ed-a84cd82591eb)  
> [FAQ](https://near.yuque.com/docs/share/57e2cfbf-d05c-4975-99fe-4eaaf11cd168)

* 支持AR内容适配刘海屏  
* 优化在低端机型上相机的帧率表现  
* 修复横屏事件偶现的加载AR内容失败的Bug  
* 修复算法定位Bug 


> 包体大小说明：  
> 静态包32位（含so）大小:约4.4M。V2签名下集成后apk体积增加4M  
静态包64位（含so）大小:约8.8M。V2签名下集成后apk体积增加8.3M  
动态包（32bit _dl包下需额外下载8.1M so）大小:约956KB。V2签名下集成后apk体积增加0.5M  

**v2.1.0**  
> 请移步**release**分支下载（或使用[Tags](https://gitlab.com/InsightAR/InsightARSDK_Android/tags)下载）

> [接入参考文档](https://near.yuque.com/docs/share/2398448a-59af-4717-9035-ccff1e9a053f)   
> [详细API参考文档](https://near.yuque.com/docs/share/2b4e012a-df65-420e-b7ed-a84cd82591eb)  
> [FAQ](https://near.yuque.com/docs/share/57e2cfbf-d05c-4975-99fe-4eaaf11cd168)

* 优化SDK包体大小；  
* 增加相机切换接口`doSwitchArCamera`；  
* 修复录屏结束回调延时问题；  
* 修复部分手机上相机画面花屏问题；  
* 修复算法Bug，优化相关识别性能；  


> 包体大小说明：  
> 静态包32位（含so）大小:约4.4M。V2签名下集成后apk体积增加4M  
静态包64位（含so）大小:约8.8M。V2签名下集成后apk体积增加8.3M  
动态包（32bit _dl包下需额外下载8.1M so）大小:约949KB。V2签名下集成后apk体积增加0.5M  

**v2.0.5**  
> 请移步**release**分支下载（或使用[Tags](https://gitlab.com/InsightAR/InsightARSDK_Android/tags)下载）

> [接入参考文档](https://near.yuque.com/docs/share/2398448a-59af-4717-9035-ccff1e9a053f)   
> [详细API参考文档](https://near.yuque.com/docs/share/2b4e012a-df65-420e-b7ed-a84cd82591eb)  
> [FAQ](https://near.yuque.com/docs/share/57e2cfbf-d05c-4975-99fe-4eaaf11cd168)

* 桌面版小纸条需要扫描两次的体验优化；  
* 增加64bit so动态下发接口支持；  
* 修复外部demo跳转页面回来后无法继续识别的问题；  
* ARScene0309 修改ARCode的检测只进行一次，避免因此产生的卡顿；  
* 更新ARCode+semiVIO的频繁输出trackingLimited状态；  
* 修复云端在线识别背景图片错误的问题；  
* 服务端支持设备黑名单策略；  

> 包体大小说明：  
> 静态包32位（含so）大小:约6.6M。V2签名下集成后apk体积增加6.2M  
静态包64位（含so）大小:约13M。V2签名下集成后apk体积增加12.5M  
动态包（需额外下载13.7M so）大小:约1M。V2签名下集成后apk体积增加0.5M  



**v2.0.0**  
> 请移步**release**分支下载（或使用[Tags](https://gitlab.com/InsightAR/InsightARSDK_Android/tags)下载）

> [接入参考文档](https://near.yuque.com/docs/share/2398448a-59af-4717-9035-ccff1e9a053f)   
> [详细API参考文档](https://near.yuque.com/docs/share/2b4e012a-df65-420e-b7ed-a84cd82591eb)  
> [FAQ](https://near.yuque.com/docs/share/57e2cfbf-d05c-4975-99fe-4eaaf11cd168)

* SDK支持扫描ARCode(作用类似于QR Code)
* 支持多人AR (Cloud Anchors)服务，可实现多人共享现实场景信息玩法
* RunScript内容协议增加网络通信type(`IAr3dEventMessage.TYPE_NETWORK_API`)，可通过该type进行动态网络协议开发
* 内容协议回调分享接口扩展，详情见Share3dEventMessage
* AR内容支持唤起输入法
* AR内容支持调起手机震动
* 引擎去除freetype，有Java层支持文字转图片
* 增加MSCKF算法支持
* Bug修复和性能优化

> 包体大小说明：  
> 静态包32位（含so）大小:约6.6M。V2签名下集成后apk体积增加6.2M  
静态包64位（含so）大小:约12.8M。V2签名下集成后apk体积增加12.5M  
动态包（需额外下载13.7M so）大小:约1M。V2签名下集成后apk体积增加0.5M  

**v1.7.1**  
> 请移步**release**分支下载（或使用[Tags](https://gitlab.com/InsightAR/InsightARSDK_Android/tags)下载）  
  
> 接入文档地址：https://ardoc.ai.163.com/  

* 适配Android Q系统

**v1.7.0**  
> 请移步**release**分支下载（或使用[Tags](https://gitlab.com/InsightAR/InsightARSDK_Android/tags)下载）  
  
> 接入文档地址：https://ardoc.ai.163.com/  

> v1.7.0版本集成SDK大小情况说明：  
> 静态包（含so）大小:5.8M。V2签名下集成后apk体积增加**5.4M**    
> 动态包（需额外下载11.9M so）大小:855K。V2签名下集成后apk体积增加**0.4M**

* 增加美颜功能，包含人脸贴纸/美颜美型/滤镜功能。
* 增加后台黑名单/白名单逻辑，可通过后台配置屏蔽不支持AR的设备（需要通过调用`isArSupportOnline()`接口使用）  
* 优化算法/引擎，修复一些Bug


**v1.6.0及以后版本**

> 注意：接口集成和之前v1.5版本不兼容，请移步**v1.6_Release**分支下载（或使用[Tags](https://gitlab.com/InsightAR/InsightARSDK_Android/tags)下载）

> 接入文档地址：https://ardoc.ai.163.com/

> v1.6.1版本集成SDK大小情况说明：  
> 静态包（含so）大小:5.4M。集成后apk体积增加**5M**  
> 动态包（需额外下载11.1M so）大小:724K。集成后apk体积增加**0.4M**

* 优化sdk集成方式，提供多样化的集成方式
* 添加分类算法，同一个pid内部切换算法功能
* 优化部分bug
* 去除外部引用第三方解压库

**v1.5.9**  

* 算法修复Vivo等使用自己定制化权限系统的手机，拒绝后再stopAR出现ANR的问题  

* 修复解压带有以中文命名的资源文件时解压出错的问题  

* 引擎修复GameObject重命名问题    

> v1.5.9版本集成SDK大小情况说明：  
> 静态包（含so）大小: 4.9M。集成后apk体积增加**4.2M**  
> 动态包（需额外下载7.9M so）大小：1.3M。集成后apk体积增加**0.6M**

**v1.5.8_hotfix**

* 修复少部分手机（目前已知：荣耀8青春版）AR内容点击事件响应延迟的问题


**v1.5.8(建议升级)**

* 修复使用非法app key时闪退的问题

* 修复动态下载so模式下获取数据异常的问题

* 修复部分设备兼容性问题：从后台切回来没有重启云识别

* 修复一些异常情况下算法/引擎的Crash问题

* 去掉需要外部工程依赖的apache Ant库

* 减少aar包的大小： 4.7M + ant lib size -> 4.6M

* InsightARMessage hashParam 变量增加静态常量key值（详见Demo `MainActivity`->`on3DEventMessage(InsightARMessage message)`）

* 增加主线程loadLibrary()的接口兼容逻辑

* *Demo*中增加支持传入本地资源的选项   

* *Demo*中增加强制设置屏幕横竖屏选项（**注意！如果设置的屏幕方向和资源本身的方向相反，资源会显示不正常**）




**v1.5.7**    

* 修复2D-Semi-VIO算法识别困难的问题

* 减少aar包体体积 5.2M -> 4.7M

**v1.5.6**    

* 支持2D-Semi-VIO算法

* 增加key Logo隐藏功能

* 适配荣耀7机型显示不正确的问题

**v1.5.5**    

* 单个事件隐藏logo功能

* 资源校验修改提示UI

* 拍照/录像结束后的卡顿优化

* 更新引擎so，修复一些非常规Bug及内存泄漏
 
**v1.5.4**    

* 增加app key本地校验逻辑，可离线完成SDK的使用权校验

* 增加使用本地资源时的校验逻辑     

**v1.5.3hotfix**    

* 更新渲染引擎，修复少部分机型上出现黑屏崩溃的问题     

**v1.5.3**    

* 更新渲染引擎，修复粒子系统的MeshRenderer没有计算顶点颜色的bug   

* 修复当模型不正确时引发的引擎表现问题

**v1.5.2**  

* 增加AR Sticker功能，增加拍照/录制视频控制区交互

* 提供录制AR视频功能，增加拍照/录屏完成后回调

* 增加手势识别功能，支持十种预定手势识别

* 支持64位动态库

* 改动部分外部接口，将其改为static方法，列表如下

		- static boolean isArAvailable()
		- static boolean isArSupport(Context context)
		- static boolean isLibraryPrepared(Context context)

* 1.4版本Bug修复

* 修复内存泄漏问题 (1.4版本没有该问题)

* 修复渲染引擎表现问题，优化渲染效果


**v1.5.1beta**  

* 增加Sticker功能

**v1.4.9hotfix**

* 修复解压过程中杀进程，引擎读到不完整文件闪退的问题   

* 【偶现】解决接入Fragment以后退出再次进入，InsightARPlayer变量没被销毁抛异常的问题.  

* 去掉文件读写权限，SDK内部流程不再需要该权限  

* 修复2DTracking识别成功后快速移开Crash的问题  

**v1.4.9**

* 暴露isArSupport(Context context)方法，支持本地判断机型是否支持AR功能；
* 动态下载so模式下，修复下载so未完成时调用业务接口不能使用的问题

**v1.4.8**

* 渲染引擎基于系统安全性考虑，将读取并运行脚本的函数从 能读取系统文件脚本的功能 改为 只能读取并运行指定文件夹脚本；
* 外部接口方法与1.4.7保持一致。
* 优化：安全性

**v1.4.7**  

* IMU数据处理变动，更新算法，修复模型放置位置错误问题；

* 修复算法退到后台偶现黑屏Bug

* 修复读取资源存在有效期问题；  

* 增加local key验证功能；  

* 云识别模式支持本地包路径传入使用；  

* init()方法使用本地加载模式时增加主线程回调；