package com.netease.ai.insightdemo;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.leon.lfilepickerlibrary.LFilePicker;
import com.netease.ai.insightdemo.cloudreco.DemoCloudRecoActivityJava;
import com.netease.ai.insightdemo.normalevent.DemoNormalEventActivityKotlin;
import com.netease.ai.insightdemo.normalevent.DemoNormalEventActivityJava;
import com.netease.insightar.NEArInsight;
import com.netease.insightar.exception.ArInsightLibraryNotFoundException;

import java.io.File;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class OutDemoMainActivityJava extends AppCompatActivity {

    private HashMap<String, String> eventIDs = new HashMap<>();

    public static final int REQUEST_CODE_FROM_ACTIVITY = 1;
    public static final int REQUEST_CODE_PERMISSION = 2;

    private TextView tvSdkVersion;
    private Button btnCloudDefaultActivity;
    private Button btnNormalEventDefaultActivity;
    private Button btnLocalEventActivity;
    private Button btnCheckList;

    RadioButton rbCar;
    RadioButton rbRobot;
    RadioButton rbEarth;
    private String mEventID;

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_out_demo_main1);

        // 版本中几个demo事件和id的对应关系
        eventIDs.put("car", "1180");
        eventIDs.put("robot", "1185");
        eventIDs.put("earth", "1186");

        initView();

        initCloudReco();

        initNormalEvent();
    }

    private void initView() {
        tvSdkVersion = findViewById(R.id.tvSdkVersion);
        tvSdkVersion.setText(NEArInsight.getInsightArSDKVersionName());

        rbCar = findViewById(R.id.rbCar);
        rbRobot = findViewById(R.id.rbRobot);
        rbEarth = findViewById(R.id.rbEarth);
        btnCheckList = findViewById(R.id.completeCheckList);
        btnLocalEventActivity = findViewById(R.id.btnNormalEventLocal);

        rbCar.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                setEvent(buttonView, isChecked);
            }
        });
        rbEarth.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                setEvent(buttonView, isChecked);
            }
        });
        rbRobot.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                setEvent(buttonView, isChecked);
            }
        });
    }

    private void setEvent(CompoundButton buttonView, boolean isChecked) {
        if (!isChecked) {
            return;
        }
        String tag = (String) buttonView.getTag();
        mEventID = eventIDs.get(tag);
    }

    private void initCloudReco() {
        btnCloudDefaultActivity = findViewById(R.id.btnCloudDefaultActivity);
        btnCloudDefaultActivity.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                launchEvent(new Runnable() {
                    @Override public void run() {
                        startActivity(
                            new Intent(OutDemoMainActivityJava.this, DemoCloudRecoActivityJava.class));
                        //                        DefaultCloudRecoActivity.start(OutDemoMainActivityJava.this);
                    }
                });
            }
        });

        btnNormalEventDefaultActivity = findViewById(R.id.btnNormalEventDefaultActivity);
        btnNormalEventDefaultActivity.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                launchNomalEvent(new Runnable() {
                    @Override public void run() {
                        DemoNormalEventActivityJava.start(OutDemoMainActivityJava.this, mEventID);
                    }
                });
            }
        });
        btnLocalEventActivity.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                if (ContextCompat.checkSelfPermission(OutDemoMainActivityJava.this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(OutDemoMainActivityJava.this,
                        new String[] { Manifest.permission.WRITE_EXTERNAL_STORAGE },
                        REQUEST_CODE_PERMISSION);
                    return;
                }
                new LFilePicker().withActivity(OutDemoMainActivityJava.this)
                    .withMutilyMode(false)
                    .withChooseMode(false)
                    .withRequestCode(REQUEST_CODE_FROM_ACTIVITY)
                    .start();
            }
        });
        btnCheckList.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View view) {
                startActivity(new Intent(OutDemoMainActivityJava.this, CheckListActivity.class));
            }
        });
    }

    @Override protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_CODE_FROM_ACTIVITY) {
                //If it is a folder selection mode, you need to get the folder path of your choice
                String path = data.getStringExtra("path");
                File resourceFile = new File(path);
                if (!resourceFile.isDirectory()) {
                    Toast.makeText(getApplicationContext(), "路径" + path + "需要是文件夹",
                        Toast.LENGTH_SHORT).show();
                    return;
                }
                List<String> fileList = Arrays.asList(resourceFile.list());
                if (fileList.contains("scene") && fileList.contains("config")) {
                    DemoNormalEventActivityKotlin.Companion.start(this, "000", false, path, false);
                    return;
                }
                Toast.makeText(getApplicationContext(), "选择路径中需要包含config和scene两个文件夹",
                    Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
        @NonNull int[] grantResults) {
        if (requestCode == REQUEST_CODE_PERMISSION) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                new LFilePicker().withActivity(OutDemoMainActivityJava.this)
                    .withMutilyMode(false)
                    .withChooseMode(false)
                    .withRequestCode(REQUEST_CODE_FROM_ACTIVITY)
                    .start();
            } else {
                Toast.makeText(this, "本地模式需要授予读写权限", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void launchNomalEvent(Runnable runnable) {
        if (TextUtils.isEmpty(mEventID)) {
            Toast.makeText(this, "请选择事件...", Toast.LENGTH_SHORT).show();
            return;
        }
        launchEvent(runnable);
    }

    private void launchEvent(Runnable runnable) {
        if (!NEArInsight.isArSupport(this)) {
            Toast.makeText(this, "手机不支持ar", Toast.LENGTH_LONG).show();
            return;
        }

        try {
            NEArInsight.checkArLibrary();
            runnable.run();
        } catch (ArInsightLibraryNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void initNormalEvent() {
    }
}
