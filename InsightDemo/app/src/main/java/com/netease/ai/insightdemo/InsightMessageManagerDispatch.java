package com.netease.ai.insightdemo;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Environment;
import android.os.SystemClock;
import android.support.annotation.StringRes;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.netease.insightar.InsightConstants;
import com.netease.insightar.NEArView;
import com.netease.insightar.commonbase.widgets.web.NEAIWebActivity;
import com.netease.insightar.entity.message.ExecuteScript3dEventMessage;
import com.netease.insightar.entity.message.OpenUrl3dEventMessage;
import com.netease.insightar.entity.message.Reload3dEventMessage;
import com.netease.insightar.entity.message.Share3dEventMessage;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * @author zfxu
 * 洞见内容消息分发公共处理，外部可自定义自己的逻辑，这个仅仅是为了展示功能如何使用
 */
public class InsightMessageManagerDispatch {

    private static final String TAG = InsightMessageManagerDispatch.class.getSimpleName();
    private Activity mActivity;

    public InsightMessageManagerDispatch(Activity context) {
        mActivity = context;
    }

    /**
     * 重新加载AR
     *
     * @param neArView                   ar视图控件
     * @param message                    内容发送过来的消息体model
     * @param cloudModeReconizedRunnable 如果是通用识别模式识别到具体事件后的操作
     */
    public void reloadAr(NEArView neArView, Reload3dEventMessage message,
                         ReloadArRunnable cloudModeReconizedRunnable) {
        // 说明内部回调该方法时发生错误，需要直接return
        if (!TextUtils.isEmpty(message.getErrorMessage())) {
            Log.e(TAG, "Error occurred: " + message.getErrorMessage());
            return;
        }
        // 在没有error message的情况下返回null的pid，说明是由内容触发了重载当前AR内容的行为
        if (TextUtils.isEmpty(message.getPid())) {
            neArView.doArReload();
            return;
        }
        // 带了pid，则表明当前还未有AR内容显示，需要先下载
        cloudModeReconizedRunnable.run(message.getPid());
    }

    /**
     * 执行脚本
     */
    public void executeScripts(ExecuteScript3dEventMessage message, NEArView neArView) {
        String messageToShow = message.getScriptName();
        if (TextUtils.isEmpty(messageToShow)) {
            Log.w(TAG, "executeScripts no message to show");
            return;
        }

        showToast(messageToShow);

        if (messageToShow.equals("g_ShowText")) {
            neArView.doArExecuteScript("g_ShowText(\"天下平安\")", false);
            return;
        }
        if (messageToShow.equals("g_SetUserInfo")) {
            neArView.doArExecuteScript("g_SetUserInfo (\"{\"userId\":\"a123\", \"coin\":\"5\", "
                            + "\"probability\":\"0.65\", \"bulletList\":[{\"content\":\"吐**司中了5元优惠券\"},"
                            + "{\"content\":\"悟**空中了3元优惠券\"}],\"adList\":[{\"content\":\" 百雀羚满200减20\"},{\"content\":\"风油精满100减30 \"}]}\")",
                    true);
            return;
        }
        if (messageToShow.equals("g_UpdateUserInfo")) {
            neArView.doArExecuteScript(
                    "g_UpdateUserInfo(\"{\"coin\":\"4\",\"showMenu\":\"1\",\"probability\":\"0.80\",\"situation\":\"1\"}\")",
                    true);
        }
    }

    public void screenShot(final NEArView neArView, final String pid) {
        //hard code fixme
        new Thread(new Runnable() {
            @Override
            public void run() {
                if (null == mActivity || mActivity.isFinishing()) {
                    return;
                }
                Bitmap arBitmap = neArView.getArBitmap();
                try {
                    final String fileName = Environment.getExternalStorageDirectory()
                            + "/sample_"
                            + pid
                            + "_"
                            + SystemClock.currentThreadTimeMillis()
                            + ".jpg";
                    FileOutputStream fos = new FileOutputStream(new File(fileName));
                    arBitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
                    fos.flush();
                    fos.close();
                    mActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            showToast("截图成功，已保存 -- " + fileName);
                        }
                    });
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    public void share(final Share3dEventMessage share3dEventMessage,
                      final String pid) {
        //hard code fixme
        new Thread(new Runnable() {
            @Override
            public void run() {
                final Bitmap arBitmap = share3dEventMessage.getScreenshot();
                if (arBitmap == null) {
                    mActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            showToast("分享失败 ", true);
                        }
                    });
                    return;
                }
                final String shareFilePath = getFileSavePath(pid);
                final boolean result = saveBitmapToFile(arBitmap, shareFilePath);
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (result) {
                            showToast("分享成功 " + shareFilePath, true);
                        } else {
                            showToast("分享失败 " + shareFilePath, true);
                        }
                    }
                });
            }
        }).start();
    }

    protected void showToast(String str) {
        showToast(str, true);
    }

    protected void showToast(String str, boolean isShort) {
        Toast.makeText(mActivity, str, isShort ? Toast.LENGTH_SHORT : Toast.LENGTH_LONG).show();
    }

    protected void showToast(@StringRes int str, boolean isShort) {
        Toast.makeText(mActivity, str, isShort ? Toast.LENGTH_SHORT : Toast.LENGTH_LONG).show();
    }

    private Bitmap getBrandLogoBitmap(String pathString) {
        Bitmap bitmap = null;
        try {
            File file = new File(pathString);
            if (file.exists()) {
                bitmap = BitmapFactory.decodeFile(pathString);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return bitmap;
    }

    private String getFileSavePath(String pid) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(Environment.getExternalStorageDirectory());
        stringBuilder.append(File.separator);
        stringBuilder.append("sample_");
        stringBuilder.append(pid);
        stringBuilder.append("_");
        stringBuilder.append(SystemClock.currentThreadTimeMillis());
        stringBuilder.append(".jpg");
        return stringBuilder.toString();
    }

    private boolean saveBitmapToFile(Bitmap bitmap, String path) {
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(new File(path));
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            return true;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return false;
        } finally {
            if (fos != null) {
                try {
                    fos.flush();
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * demo
     * 获取混合后的Bitmap
     *
     * @param brandIcon 品牌LOGO
     * @return 成图
     */
    private Bitmap getMixedBitmap(Context context, Bitmap brandIcon, Bitmap arBitmap) {
        // 去掉alpha通道，存在alpha通道的情况下会使透明效果的截图出问题
        arBitmap.setHasAlpha(false);
        // 纹理图旋转-90度，成为竖屏
        Matrix textureBitmapMatrix = new Matrix();
        textureBitmapMatrix.postRotate(-90);
        float scaleFactor = 1080.0f / (float) arBitmap.getWidth();
        textureBitmapMatrix.postScale(scaleFactor, scaleFactor);
        arBitmap = Bitmap.createBitmap(arBitmap, 0, 0, arBitmap.getWidth(), arBitmap.getHeight(),
                textureBitmapMatrix, true);
        int width = arBitmap.getHeight();
        int height = arBitmap.getWidth();
        if (brandIcon != null) {
            Drawable logo = new BitmapDrawable(context.getResources(), brandIcon);
            // 添加品牌slogan和logo
            int dimen26dp = YourUtil.dpToPx(context, 26);
            int bottomBoarderHeight = YourUtil.dpToPx(context, 100);
            Bitmap bitmap =
                    Bitmap.createBitmap(width + dimen26dp * 2, height + dimen26dp + bottomBoarderHeight,
                            Bitmap.Config.ARGB_8888);

            Canvas canvas = new Canvas(bitmap);
            int canvasWith = canvas.getWidth();
            Paint paint = new Paint();
            paint.setAntiAlias(true);
            paint.setColor(Color.WHITE);
            canvas.drawRect(0, 0, canvasWith, dimen26dp, paint);
            int canvasHeight = canvas.getHeight();
            int bottomBorderTop = canvasHeight - bottomBoarderHeight;
            int rightBoarderLeft = canvasWith - dimen26dp;
            canvas.drawRect(0, dimen26dp, dimen26dp, bottomBorderTop, paint);
            canvas.drawRect(rightBoarderLeft, dimen26dp, canvasWith, bottomBorderTop, paint);
            canvas.drawRect(0, bottomBorderTop, canvasWith, canvasHeight, paint);
            canvas.save();
            canvas.translate(dimen26dp, dimen26dp);
            Matrix matrix = new Matrix();
            matrix.postRotate(90);
            // 洞见水印被下面logo drawable挡住的误差值计算
            int deltaHeight = (canvasHeight - logo.getIntrinsicHeight()) - (dimen26dp + height);
            if (Math.abs(deltaHeight) < dimen26dp) {
                // 误差小于26则向上平移误差值，露出整体水印
                matrix.postTranslate(width, deltaHeight);
            } else {
                // 误差大于26，势必会改掉上面白色框部分，因此还是按原来遮盖水印的方式处理
                matrix.postTranslate(width, 0);
            }
            canvas.drawBitmap(arBitmap, matrix, null);
            int left;
            canvas.restore();
            width = logo.getIntrinsicWidth();
            height = logo.getIntrinsicHeight();
            left = rightBoarderLeft - width;
            logo.setBounds(left, canvasHeight - height, rightBoarderLeft, canvasHeight);
            logo.draw(canvas);
            return bitmap;
        } else {
            Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bitmap);
            Matrix matrix = new Matrix();
            matrix.postRotate(90);
            matrix.postTranslate(width, 0);
            canvas.drawBitmap(arBitmap, matrix, null);

            return bitmap;
        }
    }

    public void closeAr() {
        mActivity.finish();
    }

    public void openUrl(@NotNull OpenUrl3dEventMessage message) {
        Intent intent = new Intent(mActivity, NEAIWebActivity.class);
        intent.putExtra(InsightConstants.AR_REDIRECT_URL, message.getOpenUrl());
        mActivity.startActivity(intent);
    }

    public interface ReloadArRunnable {
        void run(String pid);
    }
}
