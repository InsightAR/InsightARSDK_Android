package com.netease.ai.insightdemo.sticker;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.netease.ai.insightdemo.R;
import com.netease.insightar.DefaultInsightStickerFragment;

public class DemoStickerActivity1 extends AppCompatActivity {


    public static void start(Context context) {
        Intent intent = new Intent(context, DemoStickerActivity1.class);
        context.startActivity(intent);
    }

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demo_sticker1);

        getSupportFragmentManager().beginTransaction()
            .replace(R.id.fragmentParent, new DefaultInsightStickerFragment())
            .commit();
    }
}
