package com.netease.ai.insightdemo.sticker;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.netease.ai.insightdemo.R;

/**
 * @author zfxu
 */
public class DemoStickerActivity2 extends AppCompatActivity {


    public static void start(Context context) {
        Intent intent = new Intent(context, DemoStickerActivity2.class);
        context.startActivity(intent);
    }

    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demo_sticker2);

        getSupportFragmentManager().beginTransaction()
            .replace(R.id.fragmentParent, new DemoStickerFragment())
            .commit();
    }
}
