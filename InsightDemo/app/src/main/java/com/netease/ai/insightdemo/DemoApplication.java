package com.netease.ai.insightdemo;

import android.app.Application;

import com.netease.insightar.NEArInsight;

/**
 * demo application
 */

public class DemoApplication extends Application {

    @Override
    public void onCreate() {

        super.onCreate();

        //外部的appkey以及 secret
        String defaultAppKey = "AR-9F6F-6EB43025342B-a-t";
        String defaultAppSecret = "OL3ihE5F4n";
        NEArInsight.init(this, defaultAppKey, defaultAppSecret, false);

    }
}
