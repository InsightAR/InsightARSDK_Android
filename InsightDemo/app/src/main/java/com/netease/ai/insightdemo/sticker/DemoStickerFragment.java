package com.netease.ai.insightdemo.sticker;

import com.netease.ai.insightdemo.R;
import com.netease.insightar.DefaultInsightStickerFragment;
import com.netease.insightar.entity.ArInsightAlgResult;
import com.netease.insightar.entity.message.IAr3dEventMessage;

/**
 * @author zfxu
 */
public class DemoStickerFragment extends DefaultInsightStickerFragment {

    @Override protected int getBottomViewXml() {
        return R.layout.demo_sticker_fragment_bottom;
    }

    @Override protected void initBottomView() {

    }

    @Override public void on3dEventMessage(IAr3dEventMessage message) {

    }

    @Override public void onArEngineResult(ArInsightAlgResult result) {

    }
}
